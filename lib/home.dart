// import 'package:flutter/material.dart';

// import 'item.dart';

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   Widget _buildRow({Widget widget1, Widget widget2}) {
//     return Padding(
//       padding: EdgeInsets.symmetric(horizontal: 5),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: <Widget>[widget1, widget2],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         automaticallyImplyLeading: false,
//         title: Image.asset(
//           '',
//           width: 100,
//         ),
//         leading: IconButton(
//           onPressed: () {},
//           icon: Icon(Icons.notifications),
//         ),
//       ),
//       body: ListView(
//         children: <Widget>[
//           Container(
//             width: MediaQuery.of(context).size.width,
//             height: MediaQuery.of(context).size.height / 3,
//             decoration: BoxDecoration(
//                 image:
//                     DecorationImage(image: AssetImage(''), fit: BoxFit.cover)),
//           ),
//           _buildRow(
//               widget1: buildItem(
//                   borderColor: Color.fromRGBO(52, 176, 52, 1),
//                   title: 'انشاء عقد',
//                   context: context,
//                   containerColor: Color.fromRGBO(234, 246, 234, 1),
//                   image: 'assets/image1.png'),
//               widget2: buildItem(
//                   title: 'طلباتي',
//                   context: context,
//                   borderColor: Color.fromRGBO(201, 227, 232, 1),
//                   image: 'assets/image2.png',
//                   containerColor: Color.fromRGBO(250, 250, 250, 1))),
//           _buildRow(
//               widget1: buildItem(
//                   borderColor: Color.fromRGBO(52, 176, 52, 1),
//                   title: 'طلبات العملاء',
//                   context: context,
//                   containerColor: Color.fromRGBO(234, 246, 234, 1),
//                   image: 'assets/image3.png'),
//               widget2: buildItem(
//                   title: 'حسابي',
//                   context: context,
//                   borderColor: Color.fromRGBO(201, 227, 232, 1),
//                   image: 'assets/image4.png',
//                   containerColor: Color.fromRGBO(250, 250, 250, 1)))
//         ],
//       ),
//     );
//   }
// }
