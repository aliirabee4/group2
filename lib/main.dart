import 'package:flutter/material.dart';
import 'package:g2/session11/tapbar.dart';
import 'Clothes/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hamza',
      theme: ThemeData(
          primaryColor: Color.fromRGBO(130, 46, 61, 1),
          accentColor: Color.fromRGBO(255, 185, 196, 1)),
      home: TapBarScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
