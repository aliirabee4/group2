// import 'package:flutter/material.dart';

// class HomeSceen extends StatefulWidget {
//   @override
//   _HomeSceenState createState() => _HomeSceenState();
// }

// class _HomeSceenState extends State<HomeSceen> {
//   Widget _item({Color color, String image}) {
//     return Container(
//       width: MediaQuery.of(context).size.width / 2 - 30,
//       height: 50,
//       color: color,
//       padding: EdgeInsets.symmetric(horizontal: 20),
//       child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
//         Image.asset(image, width: 40, height: 40),
//       ]),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Stack Demo'),
//       ),
//       body: Stack(
//         children: <Widget>[
//           Container(
//             width: MediaQuery.of(context).size.width,
//             height: 200,
//             color: Colors.white,
//           ),
//           Container(
//             width: MediaQuery.of(context).size.width,
//             height: 150,
//             color: Colors.red,
//           ),
//           Positioned(
//               top: 125,
//               left: 20,
//               right: 20,
//               child: Row(
//                 children: <Widget>[
//                   _item(color: Colors.green, image: 'assets/bell.png'),
//                   SizedBox(
//                     width: 20,
//                   ),
//                   _item(color: Colors.yellow, image: 'assets/bell.png'),
//                 ],
//               ))
//         ],
//       ),
//     );
//   }
// }
