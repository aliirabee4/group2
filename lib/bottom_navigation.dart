//import 'package:flutter/material.dart';
//import 'package:g2/bottom_navigation_scr/home.dart';
//import 'package:g2/bottom_navigation_scr/msg.dart';
//import 'package:g2/bottom_navigation_scr/notifications.dart';
//import 'package:g2/bottom_navigation_scr/serch.dart';
//
//class BottomNavigationScreen extends StatefulWidget {
//  @override
//  _BottomNavigationScreenState createState() => _BottomNavigationScreenState();
//}
//
//class _BottomNavigationScreenState extends State<BottomNavigationScreen> {
//  int _currentIndex = 0;
//  List _screens = [
//    HomeScreen(),
//    SearchScreen(),
//    NotificationsScreen(),
//    MsgScreen()
//  ];
//
//  BottomNavigationBarItem _item() {
//    return BottomNavigationBarItem(
//        icon: Icon(
//          Icons.home,
//          size: 30,
//        ),
//        title: Text(
//          '',
//          style: TextStyle(fontSize: 0),
//        ));
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      floatingActionButton: FloatingActionButton(
//        onPressed: () {},
//        child: Icon(Icons.add),
//      ),
//      bottomNavigationBar: BottomNavigationBar(
//        currentIndex: _currentIndex,
//        type: BottomNavigationBarType.fixed,
//        onTap: (index) {
//          setState(() {
//            _currentIndex = index;
//          });
//        },
//        items: [
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.home,
//                size: 30,
//              ),
//              title: Text(
//                '',
//                style: TextStyle(fontSize: 0),
//              )),
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.search,
//                size: 30,
//              ),
//              title: Text('', style: TextStyle(fontSize: 0))),
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.notifications,
//                size: 30,
//              ),
//              title: Text('', style: TextStyle(fontSize: 0))),
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.mail_outline,
//                size: 30,
//              ),
//              title: Text('', style: TextStyle(fontSize: 0)))
//        ],
//      ),
//      body: _screens[_currentIndex],
//    );
//  }
//}
