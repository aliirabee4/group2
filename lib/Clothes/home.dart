//import 'package:flutter/material.dart';
//import 'package:g2/Clothes/textField.dart';
//
//class HomeScreen extends StatefulWidget {
//  @override
//  _HomeScreenState createState() => _HomeScreenState();
//}
//
//class _HomeScreenState extends State<HomeScreen> {
//  List<Map<String, dynamic>> _dataList = [
//    {
//      'isSelected': true,
//      'image':
//          'https://image.freepik.com/free-vector/clothes-rack-with-dresses-hangers_3446-101.jpg',
//      'title': 'Men'
//    },
//    {
//      'isSelected': false,
//      'image':
//          'https://image.freepik.com/free-vector/clothes-rack-with-dresses-hangers_3446-101.jpg',
//      'title': 'Women'
//    },
//    {
//      'isSelected': false,
//      'image':
//          'https://image.freepik.com/free-vector/clothes-rack-with-dresses-hangers_3446-101.jpg',
//      'title': 'Kids'
//    },
//    {
//      'isSelected': false,
//      'image':
//          'https://image.freepik.com/free-vector/clothes-rack-with-dresses-hangers_3446-101.jpg',
//      'title': 'Watch'
//    },
//  ];
//
//  int selectedProduct = 0;
//
//  Widget _title({String title}) {
//    return Row(
//      children: [
//        Text(
//          title,
//          style: TextStyle(
//              color: Theme.of(context).primaryColor,
//              fontWeight: FontWeight.bold,
//              fontSize: 22),
//        )
//      ],
//    );
//  }
//
//  Widget _productCard(
//      {String title, String imageUrl, int index, bool isSelected}) {
//    return InkWell(
//      onTap: () {
//        setState(() {
//          _dataList[selectedProduct]['isSelected'] = false;
//          selectedProduct = index;
//          _dataList[selectedProduct]['isSelected'] = true;
//        });
//      },
//      child: Container(
//        width: 100,
//        height: 100,
//        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
//        decoration: BoxDecoration(
//            borderRadius: BorderRadius.circular(5),
//            color: isSelected
//                ? Colors.red
//                : Theme.of(context).accentColor.withOpacity(0.4)),
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceAround,
//          children: [
//            Container(
//              height: 50,
//              width: 50,
//              decoration: BoxDecoration(
//                  borderRadius: BorderRadius.circular(5),
//                  image: DecorationImage(image: NetworkImage(imageUrl))),
//            ),
//            Text(
//              title,
//              style: TextStyle(color: isSelected ? Colors.white : Colors.black),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  Widget _itemCard() {
//    return Material(
//      elevation: 5,
//      child: Stack(
//        children: [
//          Container(
//            width: MediaQuery.of(context).size.width / 2 - 20,
//            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//            height: 200,
//            decoration: BoxDecoration(
//                borderRadius: BorderRadius.circular(5), color: Colors.white),
//          ),
//          Positioned(top: 20, right: 15, child: Icon(Icons.favorite)),
//          Positioned(
//            top: 30,
//            left: 10,
//            child: Container(
//              width: MediaQuery.of(context).size.width / 4 - 10,
//              height: 120,
//              decoration: BoxDecoration(
//                  image: DecorationImage(
//                      image: NetworkImage(
//                          'https://image.freepik.com/free-vector/clothes-rack-with-dresses-hangers_3446-101.jpg'),
//                      fit: BoxFit.cover)),
//            ),
//          ),
//          Positioned(bottom: 30, left: 15, child: Text('T-Shirt')),
//          Positioned(
//              bottom: 10,
//              left: 10,
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: [
//                  Icon(
//                    Icons.star,
//                    color: Colors.yellow,
//                  ),
//                  Text('3.6'),
//                  SizedBox(
//                    width: MediaQuery.of(context).size.width / 4 - 20,
//                  ),
//                  Text('500 LE')
//                ],
//              ))
//        ],
//      ),
//    );
////      Container(
////      width: MediaQuery.of(context).size.width / 2 - 20,
////      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
////      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
////      height: 200,
////      decoration: BoxDecoration(
////          borderRadius: BorderRadius.circular(5),
////          color: Colors.grey.withOpacity(0.1)),
////      child: Column(
////        mainAxisAlignment: MainAxisAlignment.start,
////        crossAxisAlignment: CrossAxisAlignment.end,
////        children: [Icon(Icons.favorite),
////
////        ],
////      ),
////    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: ListView(
//        children: [
//          Container(
//            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
//            width: MediaQuery.of(context).size.width,
//            height: MediaQuery.of(context).size.height / 4,
//            decoration: BoxDecoration(
//                color: Theme.of(context).accentColor,
//                borderRadius: BorderRadius.only(
//                    bottomLeft: Radius.circular(25),
//                    bottomRight: Radius.circular(25))),
//            child: Column(
//              children: [
//                Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                  children: [
//                    Icon(
//                      Icons.menu,
//                      color: Theme.of(context).primaryColor,
//                    ),
//                    Icon(
//                      Icons.lock_open,
//                      color: Theme.of(context).primaryColor,
//                    )
//                  ],
//                ),
//                SizedBox(
//                  height: 15,
//                ),
//                _title(title: 'Find Your Favorite'),
//                _title(title: 'Items!'),
//                SizedBox(
//                  height: 30,
//                ),
//                Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceAround,
//                  children: [
//                    textField(context: context),
//                    Container(
//                      width: 40,
//                      height: 40,
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(30),
//                          color: Colors.white),
//                      child: Center(
//                        child: Icon(Icons.menu),
//                      ),
//                    )
//                  ],
//                )
//              ],
//            ),
//          ),
//          SizedBox(height: 20),
//          Container(
//              height: 100,
//              child: ListView.builder(
//                scrollDirection: Axis.horizontal,
//                itemCount: 4,
//                itemBuilder: (context, i) {
//                  return _productCard(
//                      index: i,
//                      title: _dataList[i]['title'],
//                      isSelected: _dataList[i]['isSelected'],
//                      imageUrl: _dataList[i]['image']);
//                },
//              )),
//          Padding(
//            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: [Text('Populer'), Text('View All')],
//            ),
//          ),
//          Container(
//            height: 200,
//            child: ListView.builder(
//              scrollDirection: Axis.horizontal,
//              itemCount: 10,
//              itemBuilder: (ctx, i) {
//                return _itemCard();
//              },
//            ),
//          )
//        ],
//      ),
//    );
//  }
//}
