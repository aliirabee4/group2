//import 'package:flutter/material.dart';
//
//class HomeScreen extends StatefulWidget {
//  @override
//  _HomeScreenState createState() => _HomeScreenState();
//}
//
//class _HomeScreenState extends State<HomeScreen> {
//  Widget _item({String title, Widget icon}) {
//    return Padding(
//      padding: EdgeInsets.symmetric(horizontal: 15),
//      child: Row(
//        children: <Widget>[
//          icon,
//          SizedBox(
//            width: 15,
//          ),
//          Text(title)
//        ],
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        elevation: 0,
//        backgroundColor: Colors.white,
//        centerTitle: true,
//        title: Image.network(
//          'https://t3.ftcdn.net/jpg/03/04/55/12/240_F_304551232_OwTLSaAoFYDZqahtE9WRkKIoc28nXscm.jpg',
//          width: 100,
//          height: 40,
//        ),
//        leading: Container(
//          width: 30,
//          height: 30,
//          margin: EdgeInsets.all(10),
//          decoration: BoxDecoration(
//              image: DecorationImage(
//                image: NetworkImage(
//                    'https://as2.ftcdn.net/jpg/03/32/59/65/500_F_332596535_lAdLhf6KzbW6PWXBWeIFTovTii1drkbT.jpg'),
//              ),
//              borderRadius: BorderRadius.circular(30),
//              color: Colors.blue),
//        ),
//      ),
//      drawer: Drawer(
//        child: ListView(children: <Widget>[
//          Row(children: <Widget>[
//            Container(
//              width: 80,
//              height: 80,
//              margin: EdgeInsets.all(10),
//              decoration: BoxDecoration(
//                  image: DecorationImage(
//                    image: NetworkImage(
//                        'https://as2.ftcdn.net/jpg/03/32/59/65/500_F_332596535_lAdLhf6KzbW6PWXBWeIFTovTii1drkbT.jpg'),
//                  ),
//                  borderRadius: BorderRadius.circular(70),
//                  color: Colors.blue),
//            ),
//          ]),
//          Row(
//            children: <Widget>[
//              Padding(
//                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//                  child: Text('Hamza'))
//            ],
//          ),
//          Row(
//            children: <Widget>[
//              Padding(
//                  padding: EdgeInsets.symmetric(
//                    horizontal: 20,
//                  ),
//                  child: Text('@Hamza_Mohamed'))
//            ],
//          ),
//          Row(
//            children: <Widget>[
//              Padding(
//                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
//                  child: Text('1000 Following')),
//              Text('1000 Followers')
//            ],
//          ),
//          Container(
//            margin: EdgeInsets.symmetric(vertical: 10),
//            height: 2,
//            color: Colors.black12,
//          ),
//          _item(title: 'Profile', icon: Icon(Icons.person))
//        ]),
//      ),
//    );
//  }
//}
