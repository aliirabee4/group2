import 'package:flutter/material.dart';
import 'package:g2/session11/calls.dart';
import 'package:g2/session11/camera.dart';
import 'package:g2/session11/chats.dart';
import 'package:g2/session11/status.dart';

class TapBarScreen extends StatefulWidget {
  @override
  _TapBarScreenState createState() => _TapBarScreenState();
}

class _TapBarScreenState extends State<TapBarScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.green,
        title: Text('WhatsApp'),
        bottom: TabBar(
          controller: _tabController,
          onTap: (index) {},
//          isScrollable: true,
//        labelColor: Colors.red,
          unselectedLabelColor: Colors.black,
          labelPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          tabs: [
            Icon(Icons.camera_alt),
            Text('Chats'),
            Text('Status'),
            Text('Calls'),
          ],
        ),
      ),
      body: TabBarView(controller: _tabController, children: [
        CameraScreen(),
        ChatsScreen(),
        StatusScreen(),
        CallsScreen()
      ]),
    );
  }
}
