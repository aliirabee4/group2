import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  File _pickedImage;
  final _picker = ImagePicker();

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
//    _getImage();
    super.initState();
  }

  Future _getImage() async {
    final _image = await _picker.getImage(source: ImageSource.camera);
    setState(() {
      _pickedImage = File(_image.path);
    });
  }

  @override
  Widget build(BuildContext context) {
//    _globalKey.currentState.openDrawer();
    return Scaffold(
        key: _globalKey,
        body: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 200,
                ),
                InkWell(
                  onTap: () {
                    _getImage();
                  },
                  child: Container(
                    width: 200,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.green),
                    child: Center(
                      child: Text('Get Camera Image'),
                    ),
                  ),
                )
              ],
            ),
            _pickedImage == null
                ? Container()
                : Container(
              width: 200,
              height: 200,
              decoration: BoxDecoration(
                  image: DecorationImage(image: FileImage(_pickedImage))),
            )
          ],
        ));
  }
}
